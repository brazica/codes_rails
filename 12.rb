# case - Brincadeira jogo futebol

tempo_atual = ARGV.first.to_i

puts "tipo do parametro => #{tempo_atual.class}"

# 0... 45, 46... 90
case tempo_atual
    when 0.. 45 then puts('primeiro tempo')
    when 46.. 90 then puts('segundo tempo')
    else puts('tempo invalido')
end

#$ ruby 12.rb 46
#tipo do parametro => Integer
#segundo tempo
