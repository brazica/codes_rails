# Guardando valor boleano

nome = 'renan'

autorizado = nome == 'jao'

if autorizado == 'renan'
  puts 'autorizado'
else
  puts 'nao autorizado'
end

=begins verificando

irb(main):061:0> autorizado = nome != 'jao'
=> true
irb(main):062:0> autorizado = nome == 'jao'
=> false

=end
