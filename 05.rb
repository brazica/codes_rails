# Guardando valor boleano

nome = 'Pedro'

autorizado = nome == 'Pedro'

#Com a exclamacao "!" negamo o booleano
if autorizado
  puts 'autorizado'
else
  puts 'nao autorizado'
end

#autorizado
