# case - Nao mostrar a senha usuario
nome            = ARGV.first
# A senha informada tem que ser igual a senha digitada
senha_informada = ARGV[1]

senha_registrada = case nome
            when 'renan' then 's1'
            when 'gonsalves' then  's2'
            when 'silva' then 's3'
           end

autorizado = senha_informada == senha_registrada

if autorizado
  puts 'autorizado'
else
  puts 'nao autorizado'
end

#
#$ ruby 11.rb  renan s1
#autorizado

# Con outra senha nao funfa

#$ ruby 11.rb  silva s1
# nao autorizado
