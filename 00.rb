# Console interativo

#### Interpretador
$ irb

#### Variaveis infere tipos dinamicos
nome = "renan"
=> "renan"
nome.class
=> String

#### Atribuicao
irb(main):003:0> nome = 10
=> 10
irb(main):004:0> nome
=> 10

#### TOdos melhodos da classe string
irb(main):009:0> String.public_methods
=> [:try_convert, :new, :allocate, :superclass, :<=>, :<=, :>=, :==, :===, :autoload?, :autoload, :included_modules, :include?, :name, :ancestors, :attr, :attr_reader, :attr_writer, :attr_accessor, :instance_methods, :public_instance_methods, :protected_instance_methods, :private_instance_methods, :constants, :const_get, :const_set, :const_defined?, :class_variables, :remove_class_variable, :class_variable_get, :class_variable_set, :class_variable_defined?, :public_constant, :freeze, :inspect, :deprecate_constant, :private_constant, :const_missing, :singleton_class?, :prepend, :class_exec, :module_eval, :class_eval, :include, :<, :>, :remove_method, :undef_method, :alias_method, :protected_method_defined?, :module_exec, :method_defined?, :public_method_defined?, :to_s, :public_class_method, :public_instance_method, :define_method, :private_method_defined?, :private_class_method, :instance_method, :instance_variable_set, :instance_variable_defined?, :remove_instance_variable, :instance_of?, :kind_of?, :is_a?, :tap, :instance_variable_get, :instance_variables, :method, :public_method, :singleton_method, :define_singleton_method, :public_send, :extend, :to_enum, :enum_for, :pp, :=~, :!~, :eql?, :respond_to?, :object_id, :send, :display, :nil?, :hash, :class, :singleton_class, :clone, :dup, :itself, :yield_self, :taint, :tainted?, :untrust, :untaint, :trust, :untrusted?, :methods, :frozen?, :protected_methods, :singleton_methods, :public_methods, :private_methods, :!, :equal?, :instance_eval, :instance_exec, :!=, :__send__, :__id__]

#### Hierarquia das classes
String.superclass
=> Object

##### TIpo da classe do objeto
10.class
=> Integer

#### Tamanho da string
"renan".length
=> 5

#### Ultima linha ruby interpreta como return
b(main):014:0>  def soma(a, b)
irb(main):015:1> a + b
irb(main):016:1> end
=> :soma

//Somando
soma(1, 2)
=> 3

#@### SOmando strings
irb(main):020:0> soma("renan", "gonsalves")
=> "renangonsalves"

#### Somando  a lista
rb(main):023:0> soma(lista, [10, 20])
=> [1, 2, 3, 4, 10, 20]
