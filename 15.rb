# Calculo idade com "Time" e Operador tenarios

ano_de_nascimento = ARGV.first.to_i

idade = Time.now.year - ano_de_nascimento

puts "Sua idade eh #{idade} #{idade ==1 ? "ano" : "anos"}"


#ruby 15.rb 1999
#Sua idade eh 19 anos

#ruby 15.rb 2017
#Sua idade eh 1 ano
