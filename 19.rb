# Passando limite de inicio e fim, e usando "odd"(para somente impares)

# ARGV eh um array, e podemos acessar pelos indice's
limite_min = ARGV[0].to_i
limite_max = ARGV[1].to_i

#### range
(limite_min..limite_max).each do |numero|
  puts numero if numero.odd?
end
