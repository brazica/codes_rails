# Aceitar um novo usuario

nome = ARGV.first
senha = ARGV[1]

# "OR" ||, se o nome for igual a renan ou igual a gonsalve, "AND" "&&" a senha igual super
autorizado = (nome == 'renan' || nome =='gonsalves') && senha == 'senha_super_secreta'

if autorizado
  puts 'autorizado'
else
  puts 'nao autorizado'
end

# $ruby 09.rb renan senha_super_secreta
# autorizado
